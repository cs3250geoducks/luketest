﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades
{
    class Program
    {
        static void Main(string[] args)
        {

            
            GradeBook book = new GradeBook();

            Stopwatch timer = new Stopwatch();
            long millisecondsElapsed = 0;

            book.NameChanged += new EventHandler<NameChangedEventArgs>(NameChanged);
            DelegateTester dt = new DelegateTester(book);
            book.NameChanged += new EventHandler<NameChangedEventArgs>(NameChanged2);
            
            //book.NameChanged = null;

            book.AddGrade(91);
            book.AddGrade(89.5f);
            book.AddGrade(75);

            Console.WriteLine($"{millisecondsElapsed} ms");
            book.Name = book.Name.Length > "Luke's Gradebook".Length ? "Luke's Gradebook" : book.Name;
            book.Name = "Someone else's Gradebook";
            Stopwatch sw = new Stopwatch();
            sw.Start();
            

            GradeStatistics stats = book.ComputeStatistics();
            Console.WriteLine(book.Name);
            Console.WriteLine(stats.AverageGrade);
            Console.WriteLine(stats.HighestGrade);
            WriteResult("Lowest", stats.LowestGrade);

            sw.Stop();

            Console.WriteLine(sw.ElapsedMilliseconds);



        }

        static void NameChanged(object sender, NameChangedEventArgs args)
        {
            Console.WriteLine($"Grade book changing name from {args.ExistingName} to {args.NewName}");
        }

        static void NameChanged2(object sender, NameChangedEventArgs args)
        {
            Console.WriteLine("**********");
        }

        static void WriteResult(string description, float result)
        {
            
            Console.WriteLine($"{description}: {result:F2}", description, result);
        
        }

    }
}
