﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades
{

    public enum StudentLevel
    {
        Exceptional = 0,
        AboveAverage,
        Average,
        BelowAverage,
        NeedsImprovement
    }

    public static class StudentLevelReport
    {
        public static StudentLevel LevelReturn(float grade)
        {
        if (grade >= 90.0)
            return StudentLevel.Exceptional;
        else if (grade >= 80.0)
            return StudentLevel.AboveAverage;
        else if (grade >= 70.0)
            return StudentLevel.Average;
        else if (grade >= 60.0)
            return StudentLevel.BelowAverage;
        else
            return StudentLevel.NeedsImprovement;
        }
    }
}
