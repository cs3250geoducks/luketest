﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades
{
    public class DelegateTester
    {
        GradeBook book;
        //GradeBook.NameChangedDelegate NameChanged;

        public DelegateTester(GradeBook book)
        {
            this.book = book;
            this.book.NameChanged += new EventHandler<NameChangedEventArgs>(DelegateTester2.NameChanged);
        }

        
    }

    public static class DelegateTester2
    {
        public static void NameChanged(object sender, NameChangedEventArgs args)
        {
            Console.WriteLine("test");
        }
    }
}
